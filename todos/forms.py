from todos.models import TodoList, TodoItem
from django.forms import ModelForm


class ListForm(ModelForm):
    class Meta:
        model = TodoList
        fields = [
            'name',
        ]
class ItemForm(ModelForm):
    class Meta:
        model = TodoItem
        fields = [
            'task',
            'due_date',
            'is_completed',
            'list'
        ]
