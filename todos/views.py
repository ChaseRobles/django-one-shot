from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList,TodoItem
from todos.forms import ListForm, ItemForm

def todo_list_list(request):
    lists = TodoList.objects.all()
    context = {
        'lists': lists,
    }
    return render(request, 'todos/todo_list_list.html', context)


# Create your views here.
def details(request, id):
    #items = TodoItem.objects.all()
    lists = get_object_or_404(TodoList, id=id)
    context = {
        #'items_objects': items,
        'lists_objects': lists
    }
    return render(request, 'todos/details.html', context)


def create_list(request):
    if request.method == 'POST':
        form = ListForm(request.POST)
        if form.is_valid():
            model_instance = form.save()
            form.save()
            return redirect('todo_list_detail', model_instance.pk)
    else:
        form = ListForm()
    context = {
        'form': form,
    }
    return render(request, 'todos/create.html', context)


def edit_list(request,id):
    lists = TodoList.objects.get(id=id)
    if request.method == 'POST':
        form = ListForm(request.POST, instance=lists)
        if form.is_valid():
            model_instance = form.save()
            form.save()
            return redirect('todo_list_detail', model_instance.pk)
    else:
        form = ListForm(instance = lists)
    context = {'form': form, 'lists':lists}
    return render(request,'todos/edit.html',context)


def delete_list(request,id):
  model_instance = TodoList.objects.get(id=id)
  if request.method == "POST":
    model_instance.delete()
    return redirect(todo_list_list)

  return render(request, "todos/delete.html")


def create_item(request):
    if request.method == 'POST':
        form = ItemForm(request.POST)
        if form.is_valid():
            model_instance = form.save()
            form.save()
            return redirect('todo_list_detail', id=model_instance.list.id)
    else:
        form = ItemForm()
    context = {
        'form': form,
    }
    return render(request, 'todos/create.html', context)


def edit_item(request,id):
    lists = get_object_or_404(TodoItem, id=id)
    #lists = TodoItem.objects.get(id=id)
    if request.method == 'POST':
        form = ItemForm(request.POST, instance=lists)
        if form.is_valid():
            item = form.save()
            return redirect('todo_list_detail', id=item.list.id )
    else:
        form = ItemForm(instance = lists)
    context = {'form': form}
    return render(request,'todos/edit_item.html',context)
